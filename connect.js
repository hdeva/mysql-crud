function crud() {
  var mysql = require("mysql");
  //  database connection
  var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "harshi10963",
    database: "harshidb",
  });

  con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
    // query execution
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Query Executed");
    });
    //to view output for description
    if (x === 7 || x === 8) {
      con.query(sql, function (err, result) {
        if (err) throw err;
        console.log(result);
      });
    } else {
      // view the table
      readRecord("food");
      con.query(sql, function (err, result) {
        if (err) throw err;
        console.log(result);
      });
    }
  });
  /**
   * sql query for creating database
   * @param {string} dbname
   */
  function createDB(dbname) {
    sql = "create database " + dbname;
  }
  /**
   * sql query for creating table
   * @param {string} tablename
   */
  function createTable(
    tablename,
    col1,
    col1_datatype,
    col2,
    col2_datatype,
    col3,
    col3_datatype,
    col4,
    col4_datatype
  ) {
    sql = ` CREATE TABLE ${tablename} (${col1} ${col1_datatype},${col2} ${col2_datatype},${col3} ${col3_datatype},${col4} ${col4_datatype})`;
  }
  // "create table food (SNo INT PRIMARY KEY,Name VARCHAR(30), Origin varchar(30),Rate int(5))";

  /**
   * sql query for inserting values into table
   * @param {string} tablename
   */
  function createRecord(tablename, col1, col2, col3, col4, col5) {
    sql = `INSERT INTO ${tablename}  VALUES (${col1},' ${col2}', '${col3}', ${col4},${col5})`;

    //1,'Panipuri','Uttar Pradesh',20,6),(2,'Idly','Tamilnadu',20,4),(3,'Pav Bhaji','Maharastra',40,1),(4,'Dosa','Karnataka',40,1),(5,'Sushi','Japan',100,1),(6,'Kimchi','Korea',80,1),(7,'Putharekulu','Andhra Pradesh',50,4),(8,'Biryani','Telangana',200,1)";
  }
  /**
   * sql query for updating table
   * @param {string} tablename
   */
  function updateTable(tablename, col5, col5_datatype) {
    sql =
      "ALTER TABLE " + tablename + " ADD COLUMN " + col5 + " " + col5_datatype;
  }
  /**
   * sql query for updating records
   * @param {string} tablename
   */
  function updateRecord(tablename,col4,col5) {
    sql = `update ${tablename} set ${col4}=50 where ${col5}>4`;
  }
  /**
   * sql query for viewing records
   * @param {string} tablename
   */

  function readRecord(tablename) {
    sql = "select * from " + tablename;
  }
  /**
   * sql query for viewing description of database
   * @param {string} dbname
   */
  function readDB(dbname) {
    sql = "use " + dbname;
  }
  /**
   * sql query for viewing description of table
   * @param {string} tablename
   */
  function readTable(tablename) {
    sql = "desc " + tablename;
  }
  /**
   * sql query for deleting records from table
   * @param {string} tablename
   */
  function deleteRecord(tablename) {
    sql = "delete from " + tablename;
  }
  /**
   * sql query for deleting table
   * @param {string} tablename
   */
  function deleteTable(tablename) {
    sql = "drop table " + tablename;
  }
  /**
   * sql query for deleting database
   * @param {string} dbname
   */
  function deleteDB(dbname) {
    sql = "drop database " + dbname;
  }
  /**
   * sql query for renaming database
   * @param {string} tablename
   */
  function updateDB(old_dbname, new_dbname) {
    // only for and before version MySQL 5.1.7
    sql = "rename database " + old_dbname + " to " + new_dbname;
  }
  let x = 5;
  var sql = "";
  switch (x) {
    case 1:
      // Create database

      createDB("harshidb");
      console.log("Database Created");

      break;

    case 2:
      // create table
      createTable(
        "food",
        "SNo",
        "INT PRIMARY KEY",
        "Name",
        "VARCHAR(30)",
        "Origin",
        "varchar(30)",
        "Rate",
        "int(5)"
      );
      console.log("Table Created");
      break;

    case 3:
      // update table
      updateTable("food", "Quantity", " int(5)");
      console.log("Table updated");
      break;
    case 4:
      // create record

      createRecord("food", 1, "Panipuri", "Uttar Pradesh", 20, 6);
      // createRecord("food", 2, "Idly", "Tamilnadu", 20, 4);
      // createRecord("food", 3, "Pav Bhaji", "Maharastra", 40, 1);
      // createRecord("food", 4, "Dosa", "Karnataka", 40, 1);
      // createRecord("food", 5, "Sushi", "Japan", 100, 1);
      // createRecord("food", 6, "Kimchi", "Korea", 80, 1);
      //  createRecord("food", 7, "Putharekulu", "Andhra Pradesh", 50, 4);
      // createRecord("food", 8, "Biryani", "Telangana", 200, 1);

      console.log("Created Records");
      break;

    case 5:
      // update record

      updateRecord("food","Rate","Quantity");
      console.log("Record updated");
      break;
    case 6:
      // read record

      readRecord("food");
      console.log("Reading Records");
      break;
    case 7:
      // read db

      readDB("harshidb");
      console.log("Reading Database");
      break;
    case 8:
      // read table

      readTable("food");
      console.log("Reading table");
      break;
    case 9:
      // delete record

      deleteRecord("food");
      console.log("Records deleted");
      break;
    case 10:
      // delete table

      deleteTable("food");
      console.log("Table deleted");

      break;
    case 11:
      // delete table

      deleteDB("harshidb");
      console.log("Database deleted");

      break;
    case 12:
      // update table

      updateDB("harshidb", "cruddb");
      console.log("Database renamed");

      break;
  }
}
crud();
