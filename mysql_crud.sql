-- creating database
create database harshidb;

-- reading database
use harshidb;

-- updating database
"rename database harshidb to cruddb";

-- deleting database
 DROP DATABASE harshidb;
 
 
-- creating table 
"create table food(
SNo INT PRIMARY KEY,
Name VARCHAR(30), 
Origin varchar(30),
Rate int(5))";

-- reading table
desc food;

-- updating table
ALTER TABLE food  ADD COLUMN Quantity int(5)";

-- deleting table
drop table food;


-- creating records
"insert into food values(1,'Panipuri','Uttar Pradesh',20,6)";
"insert into food values(2,'Idly','Tamilnadu',20,4);
"insert into food values(3,'Pav Bhaji','Maharastra',40,1)";
"insert into food values(4,'Dosa','Karnataka',40,1)";
"insert into food values(5,'Sushi','Japan',100,1)";
"insert into food values(6,'Kimchi','Korea',80,1)";
"insert into food values(7,'Putharekulu','Andhra Pradesh',50,4)";
"insert into food values(8,'Biryani','Telangana',200,1)";
  
-- reading record
SELECT * FROM food;

-- updating record
"update food set rate=30 where quantity>4";

-- deleting record
"delete from food";





